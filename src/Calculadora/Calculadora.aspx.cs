﻿using System;

namespace Calculadora
{
    public partial class Calculadora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                double num1 = double.Parse(txtPrimeiroNumero.Text);
                double num2 = double.Parse(txtSegundoNumero.Text);
                string operacao = ddlOperacao.SelectedValue;

                if (operacao == "divisao" && num2 <= 0)
                {
                    lblResult.Text = "Não é possível dividir por zero ou por um número negativo.";
                }
                else
                {
                    double resultado = Calcular(num1, num2, operacao);
                    lblResult.Text = $"Resultado da operação: {resultado}";
                }
            }
            else
            {
                lblResult.Text = "Entrada inválida. Certifique-se de preencher os números corretamente.";
            }
        }

        private double Calcular(double num1, double num2, string operacao)
        {
            switch (operacao)
            {
                case "adicao":
                    return num1 + num2;
                case "subtracao":
                    return num1 - num2;
                case "multiplicacao":
                    return num1 * num2;
                case "divisao":
                    return num1 / num2;
                default:
                    return double.NaN;
            }

        }

        private bool IsValidInput()
        {
            double num1, num2;
            return double.TryParse(txtPrimeiroNumero.Text, out num1) && double.TryParse(txtSegundoNumero.Text, out num2);
        }
    }
}