﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="Calculadora.Calculadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Calculadora</title>
</head>
<style>
    body {background-color: #e4e4e4;}
    h1, h2 {text-align: center;}
    .operacao {text-align: center; font-size: 20px;}
</style>
<body>
    <form id="form1" runat="server">
        <h1>Calculadora</h1>
        <div>
            <hr>
            <h2>Insira os dados da operação que deseja fazer!</h2>
        </div>

        <div class="operacao">
            <asp:TextBox ID="txtPrimeiroNumero" runat="server" Width="85px" Height="34px"></asp:TextBox>
            <asp:DropDownList ID="ddlOperacao" runat="server" Height="32px" Width="59px">
                <asp:ListItem Value="adicao">+</asp:ListItem>
                <asp:ListItem Value="subtracao">-</asp:ListItem>
                <asp:ListItem Value="multiplicacao">*</asp:ListItem>
                <asp:ListItem Value="divisao">/</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSegundoNumero" runat="server" Width="85px" Height="34px"></asp:TextBox>
            <br><br>
            <asp:Button ID="btnCalcular" runat="server" Text="Calcular" OnClick="btnCalcular_Click" Width="97px" Height="57px" />
            <p><asp:Label ID="lblResult" runat="server" Text="Resultado da operação:"></asp:Label></p>
        </div>
    </form>
</body>
</html>
